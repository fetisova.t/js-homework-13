/**
 * Created on 05.07.2019.
 */



const header = document.querySelector('.page-header');
const logoContainer = document.querySelector('.logo-container');
const menuItems = document.querySelector('.page-header').children;
const heroSection = document.querySelectorAll('.hero-section *');

const pageHeaderArr=[header,logoContainer, ...menuItems, ...heroSection];
console.log(pageHeaderArr);
function changeTheme() {

    pageHeaderArr.forEach((item)=>{
        item.classList.toggle('light-theme');
    });

}

let theme = localStorage.getItem('theme');
function themeCheck() {
    if(theme!==null){
        changeTheme();
        theme = localStorage.setItem('theme', 'light-theme');
    }else{
        return;
    }
}
themeCheck();

const buttonChangeTheme = document.querySelector('.change-theme');
buttonChangeTheme.addEventListener('click',()=>{
    changeTheme();

    if (header.classList.contains('light-theme')){
        theme = localStorage.setItem('theme', 'light-theme');
    }else{
        theme = localStorage.removeItem('theme');
    }

});
